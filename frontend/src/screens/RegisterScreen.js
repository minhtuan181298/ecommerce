import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { register } from '../actions/userAction';
import MessageBox from '../components/MessageBox';
import LoadingBox from '../components/LoadingBox';

function RegisterScreen(props) {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmpassword, setConfirmPassword] = useState('');


    const dispatch = useDispatch();
    const userRegister = useSelector(state => state.userRegister);
    const { userInfor, loading, error } = userRegister;

    const redirect = props.location.search ? props.location.search.split('=')[1] : '/';


    const submitHandler = (e) => {
        e.preventDefault();
        if (password !== confirmpassword) {
            alert('Password and confirm password are not the same')
        } else {
            dispatch(register(name, email, password))
        }
    };

    useEffect(() => {
        if (userInfor) {
            props.history.push(redirect)
        }
    }, [props.history, redirect, userInfor])

    return (
        <div>
            <form className="form" onSubmit={submitHandler}>
                <div>
                    <h1>Register</h1>
                </div>
                {loading && <LoadingBox></LoadingBox>}
                {error && <MessageBox variant="danger">{error}</MessageBox>}
                <div>
                    <label htmlFor="name">Name</label>
                    <input
                        type="name"
                        id="name"
                        placeholder="Enter name"
                        required
                        onChange={(e) => { setName(e.target.value) }}>
                    </input>
                </div>
                <div>
                    <label htmlFor="email">Email address</label>
                    <input
                        type="email"
                        id="email"
                        placeholder="Enter email"
                        required
                        onChange={(e) => { setEmail(e.target.value) }}>
                    </input>
                </div>
                <div>
                    <label htmlFor="password">Password</label>
                    <input
                        type="password"
                        id="password"
                        placeholder="Enter password"
                        required
                        onChange={(e) => { setPassword(e.target.value) }}>
                    </input>
                </div>
                <div>
                    <label htmlFor="confirmPassword">Password</label>
                    <input
                        type="password"
                        id="confirmPassword"
                        placeholder="Enter confirm password"
                        required
                        onChange={(e) => { setConfirmPassword(e.target.value) }}>
                    </input>
                </div>

                <div>
                    <label />
                    <button className="primary" type="submit">
                        Register
                    </button>
                </div>
                <div>
                    <label />
                    <div>Already have an account? {' '} <Link to={`/signin?redirect=${redirect}`}>Sign-In</Link></div>
                </div>
            </form>
        </div>
    );
}

export default RegisterScreen;