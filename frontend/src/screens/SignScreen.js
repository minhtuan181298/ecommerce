import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { signin } from '../actions/userAction';
import MessageBox from '../components/MessageBox';
import LoadingBox from '../components/LoadingBox';

function SignScreen(props) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const dispatch = useDispatch();
    const userSignin = useSelector(state => state.userSignin);
    const { userInfor, loading, error } = userSignin;

    const redirect = props.location.search ? props.location.search.split('=')[1] : '/';


    const submitHandler = (e) => {
        e.preventDefault();
        dispatch(signin(email, password))
    };

    useEffect(() => {
        if (userInfor) {
            props.history.push(redirect)
        }
    }, [props.history, redirect, userInfor])

    return (
        <div>
            <form className="form" onSubmit={submitHandler}>
                <div>
                    <h1>Sign In</h1>
                </div>
                {loading && <LoadingBox></LoadingBox>}
                {error && <MessageBox variant="danger">{error}</MessageBox>}
                <div>
                    <label htmlFor="email">Email address</label>
                    <input
                        type="email"
                        id="email"
                        placeholder="Enter email"
                        required
                        onChange={(e) => { setEmail(e.target.value) }}>
                    </input>
                </div>
                <div>
                    <label htmlFor="password">Email address</label>
                    <input
                        type="password"
                        id="password"
                        placeholder="Enter password"
                        required
                        onChange={(e) => { setPassword(e.target.value) }}>
                    </input>
                </div>
                <div>
                    <label />
                    <button className="primary" type="submit">
                        Sign In
                    </button>
                </div>
                <div>
                    <label />
                    <div>New customer? {' '} <Link to={`/register?redirect=${redirect}`}>Create your account</Link></div>
                </div>
            </form>
        </div>
    );
}

export default SignScreen;