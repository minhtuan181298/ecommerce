import { createStore, compose, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { cartReducer } from './reducers/cartReducers';
import { productListReducer, productDetailsReducer } from './reducers/productReducers';
import { userSigninReducer, userRegisterReducer } from './reducers/userReducer';

const initialState = {
    userSignin: {
        userInfor: localStorage.getItem('userInfor')
            ? JSON.parse(localStorage.getItem('userInfor'))
            : null,
    },
    cart: {
        cartItems: localStorage.getItem('cartItem')
            ? JSON.parse(localStorage.getItem('cartItem'))
            : [],
        shippingAddress: localStorage.getItem('shippingAddress')
            ? JSON.parse(localStorage.getItem('shippingAddress'))
            : [],
        paymentMethod: 'PayPal'
    }
};
const reducer = combineReducers({
    productList: productListReducer,
    productDetails: productDetailsReducer,
    cart: cartReducer,
    userSignin: userSigninReducer,
    userRegister: userRegisterReducer,
})

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(
    reducer,
    initialState,
    composeEnhancer(applyMiddleware(thunk))
);

export default store;