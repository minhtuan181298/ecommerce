
import { useSelector, useDispatch } from 'react-redux';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import { signout } from './actions/userAction';

import CartScreen from './screens/CartScreen';
import HomeScreen from './screens/HomeScreen';
import ProductScreen from './screens/ProductScreen';
import SignScreen from './screens/SignScreen';
import RegisterScreen from './screens/RegisterScreen';
import ShippingAddressScreen from './screens/ShippingAddressScreen';
import PaymentMethodScreen from './screens/PaymentMethodScreen';

function App() {
  const cart = useSelector(state => state.cart);
  const { cartItems } = cart;
  const userSignin = useSelector(state => state.userSignin);
  const { userInfor } = userSignin;
  const dispatch = useDispatch();
  const signoutHandler = () => {
    dispatch(signout())
  }
  return (
    <BrowserRouter>
      <div className="grid-container">
        <header className='row'>
          <div>
            <Link className='brand' to="/">amazona</Link>
          </div>
          <div>
            <Link to="/cart">Cart
            {cartItems.length > 0 && (<span className="badge">{cartItems.length}</span>)}
            </Link>
            {
              userInfor ? (
                <div className="dropdown">
                  <Link to="#">{userInfor.name} <i className="fa fa-caret-down"></i>{''}</Link>
                  <ul className="dropdown-content">
                    <Link to="#signout" onClick={signoutHandler}>Sign Out</Link>
                  </ul>
                </div>

              ) : (<Link to="/signin">Sign In</Link>)
            }

          </div>
        </header>
        <main>
          <Route path="/signin" component={SignScreen} ></Route>
          <Route path="/register" component={RegisterScreen} ></Route>
          <Route path="/cart/:id?" component={CartScreen}></Route>
          <Route path="/product/:id" component={ProductScreen}></Route>
          <Route path="/" component={HomeScreen} exact></Route>
          <Route path="/shipping" component={ShippingAddressScreen} exact></Route>
          <Route path="/payment" component={PaymentMethodScreen} exact></Route>
        </main>
        <footer className="row center">
          All right
        </footer>
      </div>
    </BrowserRouter>
  );
}

export default App;
