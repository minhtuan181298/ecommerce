import bcrypt from 'bcryptjs';
const data = {
    users: [
        {
            name: 'tuan',
            email: 'nguyenminhtuan181298@gmail.com',
            password: bcrypt.hashSync('1234', 8),
            isAdmin: true,
        },
        {
            name: 'dang',
            email: 'nguyenminhtuan181298mta@gmail.com',
            password: bcrypt.hashSync('1234', 8),
            isAdmin: false,
        }
    ],
    products: [
        {
            name: 'Nike Slim shoes',
            category: 'shoes',
            image: '/images/p1.jpg',
            price: 120,
            countInStock: 10,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 10,
            description: 'high quality product',
        },
        {
            name: 'Nike  shoes',
            category: 'shoes',
            image: '/images/p2.jpg',
            price: 120,
            countInStock: 10,
            brand: 'Nike',
            rating: 3.5,
            numReviews: 10,
            description: 'high quality product',
        },
        {
            name: 'Adidas shoes',
            category: 'shoes',
            image: '/images/p3.jpg',
            price: 120,
            countInStock: 0,
            brand: 'Nike',
            rating: 4.8,
            numReviews: 10,
            description: 'high quality product',
        }, {
            name: 'Pooz  shoes',
            category: 'shoes',
            image: '/images/p4.jpg',
            price: 120,
            brand: 'Nike',
            countInStock: 10,
            rating: 4.5,
            numReviews: 10,
            description: 'high quality product',
        },
        {
            name: 'Nike Adidaa',
            category: 'shoes',
            image: '/images/p5.jpg',
            price: 120,
            countInStock: 10,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 10,
            description: 'high quality product',
        },
        {
            name: 'yz  shoes',
            category: 'shoes',
            image: '/images/p6.jpg',
            countInStock: 10,
            price: 120,
            brand: 'Nike',
            rating: 5,
            numReviews: 10,
            description: 'high quality product',
        }

    ]
}
export default data;