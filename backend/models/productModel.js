import mongoose from 'mongoose';

const productSchema = new mongoose.Schema({
    name: { type: String, requied: true },
    image: { type: String, requied: true },
    brand: { type: String, requied: true },
    category: { type: String, requied: true },
    discription: { type: String, requied: true },
    price: { type: Number, requied: true },
    countInStock: { type: Number, requied: true },
    rating: { type: Number, requied: true },
    numReviews: { type: Number, requied: true },
},
    {
        timestamp: true,
    }
);

const Product = mongoose.model('Product', productSchema);

export default Product;