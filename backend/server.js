import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import userRouter from './router/userRouter.js';
import productRouter from './router/productRouter.js';


dotenv.config();
mongoose.connect(process.env.MONGODB_URL || 'mongodb+srv://tuan1234:tuan1234@cluster0.fk4r5.mongodb.net/amazo?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
})

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use('/api/users', userRouter);
app.use('/api/products', productRouter);

app.use((err, req, res, next) => {
    res.status(500).send({ message: err.message });
})

app.get('/', (req, res) => {
    res.send('Server is ready')
});

const port = process.env.PORT || 5000;
app.listen(port, () => {
    console.log(`Server ar http://localhost:${port}`)
});